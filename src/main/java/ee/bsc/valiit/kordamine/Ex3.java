package ee.bsc.valiit.kordamine;

public class Ex3 {
    public static void main(String[] args) {
        StringBuilder symbol = new StringBuilder();
        StringBuilder space = new StringBuilder("     ");
        for (int i = 5; i > 0; i--) {
            symbol.append("@");
            space.deleteCharAt(space.length() - 1);
            String row = space.toString() + symbol.toString();
            System.out.println(row);
        }
    }
}
