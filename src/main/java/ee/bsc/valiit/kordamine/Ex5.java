package ee.bsc.valiit.kordamine;

public class Ex5 {
    public static void exam(String name, int points) {
        int grade;
        StringBuilder pass = new StringBuilder("PASS - ");
        if (points >= 51 && points <= 60) {
            grade = 1;
        } else if (points >= 61 && points <= 70) {
            grade = 2;
        } else if (points >= 71 && points <= 80) {
            grade = 3;
        } else if (points >= 81 && points <= 90) {
            grade = 4;
        } else if (points >= 91 && points <= 100) {
            grade = 5;
        } else {
            grade = 0;
        }

        if (points < 51) {
            System.out.println("FAIL");
        } else {
            pass.append(String.format("%s, %s", grade, points));
            System.out.println(pass);
        }
    }

    public static void main(String[] args) {
        exam("Katre", 61);

    }
}
