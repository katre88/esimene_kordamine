package ee.bsc.valiit.kordamine;

public class Ex1 {
    public static void main(String[] args) {
        StringBuilder symbol = new StringBuilder("#######");
        for (int i = 6; i > 0; i--) {
            symbol.deleteCharAt(symbol.length() - 1);
            System.out.println(symbol);
        }
    }
}
