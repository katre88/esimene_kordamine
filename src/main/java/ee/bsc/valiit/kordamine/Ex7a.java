package ee.bsc.valiit.kordamine;

public class Ex7a {
    public static void main(String[] args) {
        String[][] countries = {{"Estonia", "Tallinn", "Jüri Ratas"}, {"Latvia", "Riga", "Māris Kučinskis"}, {"Lithuania", "Vilnius", "Saulius Skvernelis"}, {"Finland", "Helsinki", "Juha Sipilä"}, {"Sweden", "Stockholm", "Stefan Löfven"}, {"Spain", "Madrid", "Mariano Rajoy"}, {"Croatia", "Zagreb", "Andrej Plenković"}, {"Denmark", "Copenhagen", "Lars Løkke Rasmussen"}, {"France", "Paris", "Édouard Philippe"}, {"Norway", "Oslo", "Erna Solberg"}};
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i][2]);
        }
        System.out.println();
        for (int j = 0; j < countries.length; j++) {
            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s", countries[j][0], countries[j][1], countries[j][2]));
        }


    }
}
