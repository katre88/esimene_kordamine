package ee.bsc.valiit.kordamine;

public class Ex7b {
    public static void main(String[] args) {
        String[][][] countries = {{{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"}}, {{"Latvia"}, {"Riga"}, {"Māris Kučinskis"}, {"Latvian", "Russian", "Ukrainian", "Belarusian"}}, {{"Lithuania"}, {"Vilnius"}, {"Saulius Skvernelis"}, {"Lithuanian", "Russian", "Ukrainian", "Belarusian", "Polish"}}, {{"Finland"}, {"Helsinki"}, {"Juha Sipilä"}, {"Finnish", "Swedish", "Russian"}}, {{"Sweden"}, {"Stockholm"}, {"Stefan Löfven"}, {"Swedish", "Finnish", "Meänkieli", "Sami"}}, {{"Spain"}, {"Madrid"}, {"Mariano Rajoy"}, {"Spanish", "Basque", "Catalan"}}, {{"Croatia"}, {"Zagreb"}, {"Andrej Plenković"}, {"Croatian", "Czech", "Hungarian", "Italian"}}, {{"Denmark"}, {"Copenhagen"}, {"Lars Løkke Rasmussen"}, {"Danish", "Faroese", "German", "Swedish"}}, {{"France"}, {"Paris"}, {"Édouard Philippe"}, {"French", "English", "Spanish", "German"}}, {{"Norway"}, {"Oslo"}, {"Erna Solberg"}, {"Norwegian", "Sami", "English"}}};
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i][0][0] +":");
            for (String j: countries[i][3]) {
                System.out.println("\t" + j);
            }


        }
    }
}
